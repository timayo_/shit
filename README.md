# Migration Notice

I've moved to a new Git host for my public remote repositories. I will no longer be maintaining code featured on these repositories. You can find the new repo for this project at: https://git.gay/timayo/shit/

The original readme for this repo is below:

---

# sHit

##### It's pronounced "s-hit".

## Introduction

The simple HTML insertion tool (sHit) is static website generator script that inserts HTML snippets from other files ("templates") into your main HTML documents ("sources"), allowing you to re-use the same elements across different pages.

You could use this to insert the same header or footer across all of your pages, create templates different types of page content, or even write your entire webpage inside a template, if that's your kind of thing.

The script simply inserts the contents of whatever template file is referenced in a source document starting from the line that the reference is made, meaning you can pretty much insert any valid HTML code at any place inside your source documents.

## Installation
All of sHit's functionality is contained within a single file. Simply clone it to wherever is most convenient (it's designed to function correctly from both the working directory and any other $PATH directory).
- Obviously, you will need Python installed in order to execute the script.
### Linux
- To install at path (recommended): 
```bash
sudo wget -P /usr/local/bin 'https://gitlab.com/timayo_/shit/-/raw/main/shit.py' -O shit.py
sudo chmod +x /usr/local/bin shit.py
```

### Windows
#### "I don't know how to download from Git!!"
1. Right-click on [this link](https://gitlab.com/timayo_/shit/-/raw/main/shit.py) and select "Save Link As..."
2. Download it and place it inside an appropriate folder (e.g. the folder that contains the files you're using to create your website)
3. Run the script whenever you need to rebuild your webpages

#### Advanced Installation
- Run ```wget 'https://gitlab.com/timayo_/shit/-/raw/main/shit.py'``` in an appropriate directory.
- To be able to execute the script from anywhere, you must add the location of the file to Window's `Path` environment variable.

## Updating
To update the script to the latest version, simply repeat the installation instructions.

## Usage
During first run, you will be asked for three directories:
- A `source` directory, where your webpages will be read from
- A `template` directory, where the script will fetch the code to be inserted into your source webpages
- A `build` directory, where the fully assembled webpages will be placed.

If sHit is executed in a directory with a Git repository present, you will also be asked if you wish to exclude the cache and config files from appearing in your remote repositories (highly recommended!**

### Writing Templates
**Templates** are HTML files that contain standard HTML code.
They must be structured just as they would need to be if they were included in your main files (hopefully that was obvious.)
For example, see this template file `header.html`:
![An example of a template file.](assets/example01.png)

### Including Templates
In order to include templates in your final webpages, you must indicate which templates are to be included where inside your **source** documents.
sHit looks for a specific text marker contained within a HTML comment (by default, `inc-`).
The template to be included is identified by a string immediately proceeding the marker, as seen below:
![An example of a template included in a source file.](assets/example02.png)

### Building
Executing the script in the root directory of your project (where your config file is located) will rebuild all accessible source webpages using all available (and required) templates. Make sure to do so whenever you wish to view/publish your complete webpages.

## In Closing...
This project is my first large-scale programming project that I completed in one afternoon. It is certainly not perfect, nor the most feature complex, nor the most robust SSG out there. But it does do an okay job at one thing. It's good enough for my use, and I hope others can find use of it too (:

If you have something to suggest, report, or contribute to this project, please feel free to [open an issue](https://gitlab.com/timayo_/shit/-/issues/new), or [contact](https://timayo.gay/contact.html) me directly.
