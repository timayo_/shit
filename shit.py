#!/usr/bin/env python

## Importing necessary modules
import fileinput
import os
import re
import shutil
import sys

## Adding CWD to Python path if the script is not in the CWD
if os.getcwd != os.path.dirname(os.path.realpath(__file__)):
    sys.path.insert(1, os.getcwd())

## Very super important variables here.
## They're used substantially throughout this script, so I've included them up here for easy access.
## I've labelled each variable appropriately so you know what it's for and whether or not you should change it.

## The name of the file used for storing some configuration data. Feel free to change this.
## Default: shit_conf.py
config = 'shit_conf.py'

## Regex for the type of file the script looks for.
## You could change this, but this tool IS designed for manipulating html code so...
## Default: (.*html$)
file_type = re.compile("(.*html$)")

## The string that the script looks for in your source files to identify where to insert templates.
## If you wish to change what string is used in your code, please change it here too.
marker = "<!-- inc-"

## ---------------------------------------------------------------------- ##

## As mentioned above, this script stores some data in a configuration file.
## This code helps the user create one if the file cannot be found.
## It also asks if the user wants to automatically exclude the config files from being detected by Git, if applicable.

## Test for if there is a valid config file. If not, make one
if not os.path.isfile(config):
    print("No config file found. Generating...")

    ## If a git repository is detected, offer to add the config file to .gitignore
    if os.path.exists(".git"):
        git_check = 1
        while git_check == 1:
            ignore_conf = input("You seem to be inside a git repository. Would you like Git to ignore sHit? (Y/n): ")
            if ignore_conf == "Y" or ignore_conf == "y" or ignore_conf == "":
                if not os.path.isfile(".gitignore"):
                    with open(".gitignore", "w") as gitfile:
                        gitfile.write("# sHit config file\n" + config)
                        gitfile.write("\n# sHit cache\n__pycache__")
                        gitfile.write("\n# sHit executable\n" + os.path.basename(__file__))
                        gitfile.close()
                    print("Added script files to .gitignore.\n")
                else:
                    with open(".gitignore", "r") as gitfile:
                        gitfile_search = gitfile.read()
                        if gitfile_search.find(config) != -1:
                            print(config, "already mentioned in .gitignore. Skipping...\n")
                        else:
                            gitfile = open(".gitignore", "a")
                            gitfile.write("# sHit config file\n" + config)
                            gitfile.write("\n# sHit cache\n__pycache__")
                            gitfile.write("\n# sHit executable\n" + os.path.basename(__file__))
                            print("Added script files to .gitignore.\n")
                            gitfile.close()
                git_check = 0
            elif ignore_conf == "N" or ignore_conf == "n":
                git_check = 0
            else:
                print("Invalid option.")

    ## Ask for a source directory
    source = input("Please specify a source directory (leave blank for default): ")
    ## Set default if no option provided
    if source == "":
        source = "source"
    ## Create directory if not found
    if not os.path.exists(source):
        print("Directory not found. Creating...")
        os.makedirs(source)
    else:
        print("Directory found.")

    ## Ask for a template directory
    template = input("Please specify a template directory (leave blank for default): ")
    ## Set default if no option provided
    if template == "":
        template = "templates"
    # Create directory if not found
    if not os.path.exists(template):
        print("Directory not found. Creating...")
        os.makedirs(template)
    else:
        print("Directory found.")

    ## Ask for a build directory
    build = input("Please specify a directory to place built files (leave blank for default): ")
    ## Set default if no option provided
    if build == "":
        build = "build"
    ## Create directory if not found
    if not os.path.exists(build):
        print("Directory not found. Creating...\n")
        os.makedirs(build)
    else:
        print("Directory found.\n")

    ## Create config file in CWD
    with open(config, "w") as cfile:
        ## Add source directory to config
        cfile.write("source='" + source + "'\n")
        ## Add template directory to config
        cfile.write("template='" + template + "'\n")
        ## Add build directory to config
        cfile.write("build='" + build + "'")
        cfile.close()

## Import config data
data = __import__(re.sub(".py", "", config))

## ---------------------------------------------------------------------- ##

## Hey! Before you scroll, I just want to give you my thanks for viewing, editing or contributing to this code.
## This was one of the first large-scale programming projects I ever attempted, so chances are it's far from perfect.
## I've tried to document it as best as I can so you can get a good idea as to what's going on.
## If you're planning on using it to help build your own website, I hope you enjoy!
## Personal websites are a brilliant personal project, and I wish you well on your journey of learning and expression.
## If you intend on contributing or fixing my code, then... I'm sorry. :P

## ---------------------------------------------------------------------- ##

## In order to do anything to any of the files, those files need to be identified.
## Here I searched for all of the source and template files and added them to lists.

## Compiling lists of all source and template files
sources = []
templates = []
## Sources:
for root, dirs, files in os.walk("./" + data.source):
    for file in files:
        if file_type.match(file):
            sources.append("./" + data.source + "/" + file)
## Kill if no sources are found
if len(sources) == 0:
    print("No source files found. Terminating...")
    quit()
print("Source files found:")
## Print found source files
for source_file in sources:
    print(source_file, end = "  ")
## Templates:
for root, dirs, files in os.walk("./" + data.template):
    for file in files:
        if file_type.match(file):
            templates.append("./" + data.template + "/" + file)
## Kill if no sources are found
if len(templates) == 0:
    print("\n\nNo template files found. There is nothing to do.")
    quit()
print("\n\nTemplate files found:")
for template_file in templates:
    print(template_file, end = "  ")
print("\n")

## ---------------------------------------------------------------------- ##

## This is where the magic happens. The following code is responsible for:
## - Searching for the marker string in each source file
## - Identifying and searching for the requested template
## - Inserting all of the template code in place of the marked line in a build file

## Repeat the build process for all available source files
for page in sources:
    print("building", page + "...")
    ## Open and save the contents of the source file
    with open(page, "r", encoding="utf-8") as source_page:
        source_code = source_page.readlines()
    ## Open the equivalent build file
    build_page = open(re.sub(data.source, data.build, page), "w", encoding="utf-8")
    ## Scan each line of source code for the marker
    ## YES I know this is certainly highly inefficient but for smaller websites it's still pretty quick so idgaf
    ## I am NOT changing it until it doesn't work at all anymore
    for number, line in enumerate(source_code, 1):
        ## If the marker is found, insert the contents of the template file in its place
        if marker in line:
            template_id = re.findall("inc-([^\s]*)", line)[0]
            template_path = "./" + data.template + "/" + template_id + ".html"
            if os.path.isfile(template_path):
                with open(template_path, "r", encoding="utf-8") as template_file:
                    for line in template_file.read():
                        build_page.write(line)
            ## Exception for if the requested template file does not exist
            else:
                print("No template", template_id + ".html, skipping...")
        ## Otherwise print the source code as normal
        else:
            build_page.write(line)
    ## Build complete!
    print("Successfully built", page + ".\n")
